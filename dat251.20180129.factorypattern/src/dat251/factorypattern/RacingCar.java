package dat251.factorypattern;

public class RacingCar extends Car {

	public String toString() {
		return "I am a Racing Car";
	}
	
}
