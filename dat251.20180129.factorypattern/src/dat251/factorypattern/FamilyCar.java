package dat251.factorypattern;

public class FamilyCar extends Car {
	
	public String toString() {
		return "I am a Family Car";
	}

}