package dat251.factorypattern;

public class Truck extends Car {
	
	public String toString() {
		return "I am a Truck";
	}

}