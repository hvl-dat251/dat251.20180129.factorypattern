package dat251.factorypattern;

public class Main {
	
	public static void main(String[] args) {
		
		CarRace race = new CarRace();
		race.run();

	}
	
}